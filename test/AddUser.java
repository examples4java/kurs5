
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import miscellaneous.Miscellaneous;
import static miscellaneous.Miscellaneous.addPersonToMap;
import static miscellaneous.Miscellaneous.showAllPersons;
import utils.FileManipulate;


/**
 * Testowa klasa sprawdzająca możliwość wykorzystania klasy Miscellaneous w innym programie
 * @author tez
 */
public class AddUser {
    public static void main(String[] args) throws IOException {
        FileManipulate.writeTextFileLines(((Supplier<List<String>>)()->{
            List<String> ls = new ArrayList<>();
            ls.add("Jacek,Mariusz,Waleczko,8888888888,888-888-888;78765,AAA673956,true");
            ls.add("Michalina,Adrianna,Grzybowa,892114256,777-777-7;+90-0-9444-22-22-2,BBB673956,false");
            ls.add("Justyna,Magdalena,Helios,563215657,999-999-999;32-2678-98-56;22425-242,CCC673956,true");
            return ls;
        }).get().stream(), "persons.csv");
        
        FileManipulate.readTextFileLines("persons.csv").forEach(s->{
            String[] person = s.split(",");
            Miscellaneous.Person p1 = Miscellaneous.createPerson(person[0], 
                    person[1], 
                    person[2], 
                    person[3], 
                    person[4].split(";"), 
                    person[5], 
                    Boolean.valueOf(person[6]));
            addPersonToMap("42-200", p1);
        });
        
        showAllPersons();
    }
}
