/*
 * LICENCJA Otwartoźródłowa - każdy może korzystać, modyfikować i robić z tym
 * kodem co tylko zechce; autor ma do niego niezbywalne prawa, jednak nikt nie  
 * musi informamować autora o jego wykorzystaniu bądź zamiarze wykorzystania. 
 * Miłym gestem będzie wspomnienie od kogo pochodzi pierwotny kod.
 */
package utils;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Klasa implementuje idee mapy przechowującej wiele wartości pod jednym kluczem.
 * Przy jej tworzeniu nie jest najważniejsza szybkość i efektywność
 * lecz mechanika działania tego typu kolekcji. Oczywiście do przechowywania
 * wartości zostały wybrane najszybsze kolecje (ArrayDeque - 
 * https://stackoverflow.com/questions/6129805/what-is-the-fastest-java-collection-with-the-basic-functionality-of-a-queue)
 * jednak powinny one zostać zastąpione przez odpowiednio zoptymalizowane
 * wykorzystanie tablic (najprostsze - najszybsze)
 * 
 * WAŻNE! Klasa powinna być oparta o interfejs Map; nie zostało to jednak 
 * zaimplementowane (brak czasu); można zrobić to jako ćwiczenie
 * @author tez
 * @param <Object> - element będący klucze; kluczem nie może być typ prosty
 * @param <V> - element dodawany pod dany klucz; wartość musi być typu obiektowego 
 */
public class MultiMap<Object,V> {
    
    /*TODO - do wykonania
    - dodać metodę zwracającą informację czy istnieje dana wartość w mapie (klasycznie oraz przez strumień)
    - dodać metodą sprawdzającą czy któraś z wartości zawiera wskazaną wartość (jej fragment)   
    - przetestować obecne zaimplementowane metody pod względem podatności na błędy
    - dodać metody wyświetlające długość mapy i/lub elementów z klucza (bez ich pobierania)
    - dodać zwrot ilość wszystkich elementów ze wszystkich kluczy
    - dodać interfejs Map oraz zaimplementować brakujące metody
    - przetstować czy mapa jest kompatybilna z innymi typami map (polimorfizm zmiennej)
    */
    
    
    MySet<Object, V> sets[];
    int mapLength;
    
    /**
     * Nadpisanie domyślnego konstruktora dającego pewność, że długość mapy wynosi zero
     */
    public MultiMap() {
        mapLength = 0;
    }

    
    /**
     * Implementacja metody dodającej weartość do mapy
     * @param key - wartość klucza (nie musi być unikatowa)
     * @param value - wartość znajdująca się pod wskazanym kluczem (dopisywana do listy)
     */
    public void put(Object key, V value) {
        if (sets != null)
            for (MySet<Object, V> s:sets) {
                if (s.key==key) {
                    s.values.add(value);
                    break;
                }
            }
        MySet<Object,V> n = new MySet<>();
        n.key=key;
        n.values.add(value);
        MySet<Object, V> tmpSet[] = sets;
        if (sets!=null) {
            sets = (MySet<Object, V>[])Array.newInstance(sets.getClass().getComponentType(), ++mapLength);
            System.arraycopy(tmpSet, 0, sets, 0, mapLength-1);
        }
        else 
            sets = new MySet[++mapLength];
        
        sets[mapLength-1] = n;
    }
    
    /**
     * Metoda usuwa wszystkie wartość wraz z podanym kluczzem
     * @param key - wartość klucza, dla którego ma nastąpić usunięcie
     */
    public void remove(Object key) {
        boolean found = false;

        for (MySet<Object,V> s:sets) {
            if (s.key==key) {
                found = true;
                break;      
            }            
        }
        if (found) {
            MySet<Object, V> tmpSet[] = sets;
            int i=0;
            sets = new MySet[--mapLength];
            for(MySet<Object,V> s:tmpSet) {
                if (s.key==key) continue;
                sets[i]=s;
            }
        }        
    }
    
    /**
     * Implementacja metody pobrania wartości spod podanego klucza
     * @param key - wartość klucza
     * @return zwracana jest tablica zapisanych wartości pod podanym kluczem
     * lub pusta kolejka (nowa)  jeżeli brak jest klucza/wartości
     */
    public Deque<V> get(Object key) {
        //TODO do zrobienia - obejcie problemu związanego z pustą tablicą (wyjątek!) 
        for (MySet<Object,V> s:sets)
            if (s.key==key) return s.values; 
        return new ArrayDeque<>();
    }
    
    /**
     * Metoda podmienia wartości pod podanym kluczem na nowe (kasuje wcześniejsze)
     * @param key - wartość klucza
     * @param value - lista nowych wartości (Deque)
     */
    public void replace(Object key, Deque<V> value) {
        replace(key,key, value);
    }
    
    /**
     * Metoda podmienia wartości ORAZ nazwę klucza 
     * @param oldKey - stara (szukana) wartość klucza
     * @param newKey - nowa wartość klucza
     * @param value - lista nowych wartości (Deque)
     */
    public void replace(Object oldKey, Object newKey, Deque<V> value) {
        int i = 0;
        while(i<sets.length) {
            if (sets[i].key==oldKey) {
                sets[i].key=newKey;
                sets[i].values.clear();
                sets[i].values.addAll(value);
                break;
            }
        }
    }
    
    /**
     * Metoda zwraca wszystkie klucze zawarte w mapie
     * @return Kolejka (Deque) ze wszystkimi wartościami kluczy
     */
    public Deque<Object> keys() {
        Deque<Object> ret = new ArrayDeque<>();
        for (MySet<Object,V> s:sets) ret.add(s.key);
        return ret;
    }
    
    /**
     * Metoda zwraca wszystkie wartości spod wszystkich kluczy w kolejce
     * @return Kolejka (Deque) ze wszystkimi wartości mapy
     */
    public Deque<V> values() {
        Deque<V> ret = new ArrayDeque<>();
        for (MySet<Object,V> s:sets) {
            for (V v:s.values) ret.add(v);
        }
        return ret;
    }
    
    /**
     * Klasa pomocnicza, której celem jest utworzenie modelu pojedynczej komórki
     * przechowującej wartość w mapie. 
     * Klasa stanowi typ tablicy, która przechowuje naszą właściwą mapę
    */
    class MySet<Object,V> {
        Object key; 
        final Deque<V> values = new ArrayDeque<>();

    }
    
}
