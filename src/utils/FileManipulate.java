/*
 * LICENCJA Otwartoźródłowa - każdy może korzystać, modyfikować i robić z tym
 * kodem co tylko zechce; autor ma do niego niezbywalne prawa, jednak nikt nie  
 * musi informamować autora o jego wykorzystaniu bądź zamiarze wykorzystania. 
 * Miłym gestem będzie wspomnienie od kogo pochodzi pierwotny kod.
 */
package utils;

import java.io.IOException;
import java.nio.file.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.*;

/**
 * Klasa ma za zadanie ułatwić operacje na plikach i systemie plików
 * @author tez
 */
public class FileManipulate {
    /*TODO Do zrobienia:
    - dodać możliwość kopiowania plików
    - dodać możliwość zachowania ścieżki katalogu, w którym aktualnie pracujemy
    - dodać usuwanie plików
    - dodać możliwość czytania i wybierania danych z plików CSV
    */
    public boolean checkFile(String f) {
        return Files.exists(Paths.get(f),LinkOption.NOFOLLOW_LINKS);
    }
    
    public static Stream<String> readTextFileLines(String f) throws IOException {
        return Files.lines(Paths.get(f));
    }
    
    public static boolean writeTextFileLines(Stream<String> content, String f) {
        try {
            //poniżej trzeba zamienić strumień na ciąg/kolekcję iteracyjną
            //w tym wypadku wyciągamy nie cały obiekt, a jedynie odnośnik
            //do pozycji iteratora (normalnie mamy do tego metodę iterator)
            //więcej: http://www.lambdafaq.org/how-do-i-turn-a-stream-into-an-iterable/
            Files.write(Paths.get(f), (Iterable<String>)content::iterator);
        } catch (IOException ex) {
            Logger.getLogger(FileManipulate.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    } 
}
