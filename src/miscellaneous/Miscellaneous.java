/*
 * LICENCJA Otwartoźródłowa - każdy może korzystać, modyfikować i robić z tym
 * kodem co tylko zechce; autor ma do niego niezbywalne prawa, jednak nikt nie  
 * musi informamować autora o jego wykorzystaniu bądź zamiarze wykorzystania. 
 * Miłym gestem będzie wspomnienie od kogo pochodzi pierwotny kod.
 */
package miscellaneous;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.FileManipulate;
import utils.MultiMap;

/**
 *
 * @author tez
 */
public class Miscellaneous {

    /*TODO DO zrobienia:
    - dodać realne połączenie osób z miastami (obecnie nie istnieje)
    - dodać możliwość zapisania aktualnego stanu dodanych miast i kodów do pliku CSV
    - dodać mozliwość zapisania osób do pliku CSV wraz z dowiązaniem do miasta
    - dorobić odpowiednie metody w klasach Person i Town pozwalające na dodawanie
    pojedynczo każdego z parametrów (oraz odbierania) 
    - dodać pełny odczyt wszystkich danych w taki sposób, by po wczytaniu danych
    z pliku można było przeglądać odpowiednio połączone osoby z miastami
    - opcjonalnie dodoać do osób ulicę i numer posesji/lokalu (mieszkania)
    
    Więcej na temat:
    - czytania plików - https://www.mkyong.com/java8/java-8-stream-read-a-file-line-by-line/
    - operacji na systemie plików - http://eherrera.net/ocpj8-notes/09-java-file-io-(NIO.2)
    - operacji wejścia/wyjścia w Java (dotyczy plików, w tym starszych wersji Java) - https://howtodoinjava.com/java8/java-8-write-to-file-example/
    - więcej o strumieniach - https://winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/
    - dyskusja o wydajności strumieni i kolekcji - https://stackoverflow.com/questions/22658322/java-8-performance-of-streams-vs-collections
    - inspiracja MultiMap - https://golb.hplar.ch/2017/09/Do-it-yourself-Multi-Value-Map-with-Java-8.html
    
    */
    
    /** 
     * Zabezpieczenie przed utworzeniem obiektu z tej klasy (konstruktor może zostać wywołany jedynie
     * przez metodę tej klasy)
     */
    private Miscellaneous() {}
    
    
    
    class Town {        
        String name, country;
        long population;
        Date foundationDate;

        Town() {
            this("","",0,new Date(System.nanoTime()));
        }
        
        Town(String name, String country, long population, Date foundationDate) {
            this.name = name;
            this.country = country;
            this.population = population;
            this.foundationDate = foundationDate;
        }       
        
                
        public String getName() {
            return name;
        }
        
        public String getDate() {
            return SimpleDateFormat.getDateTimeInstance().format(foundationDate);// foundationDate.getTime();
        }    
    }
    
    List<Town> townsName;    
    Map<String,Integer> towns;
    
    public static class Person {
        String name,secondName, surname;
        String pesel, phones[], idcard;
        boolean active;

        private Person() {
            
        }
        
        private Person(String name, String secondName, String surname, String pesel, String[] phones, String idcard, boolean active) {
            this.name = name;
            this.secondName = secondName;
            this.surname = surname;
            this.pesel = pesel;
            this.phones = phones;
            this.idcard = idcard;
            this.active = active;
        }
        
        public Map<String,Object> getPerson() {
            if (this.name == null && this.surname==null && this.pesel == null && this.idcard==null)
                try {
                    throw new Exception("This person isn't complete or doesn't exist anymore!");
                } catch (Exception ex) {
                    Logger.getLogger(Miscellaneous.class.getName()).log(Level.SEVERE, null, ex);
                    return new HashMap<>();
                }            
            return (Map<String, Object>) ((Supplier<Map<String,Object>>) () -> {
                Map<String,Object> m = new HashMap<>();
                m.put("Imię", name);
                m.put("Nazwisko",surname);
                m.put("Drugie imię", secondName);
                m.put("PESEL", pesel);
                m.put("Dowód osobisty", idcard);
                m.put("Numery telefonów", (List<String>) ((Supplier<List<String>>) () -> {
                    List<String> ls = new ArrayList<>();
                    for (String s:phones) ls.add(s);
                    return ls;
                }).get());
                m.put("Obecna aktywność", (active) ? "aktywny": "nieaktywny");
                return m;
            }).get();
        }         
    }    
    
    /**
     * Lista ma za zadanie przechowywać wszystkie dodane w trakcie działania programu osoby. 
     * 
     * @param String klucz przchowuje wartość kodu pocztowego miejscowości (odnalezienie w miastach)
     * @param Pesron nowy obiekt osoby dostępny w danym mieście 
     */
    //List<MultiHashtable<String,Person>> persons;    
    final static MultiMap<String, Person> PERSONS = new MultiMap<>();
    
    /**
     * Metoda ma za zadanie utworzyć i zwrócić obiekt klasy Person (która jest
     * prywatną klasą Miscellaneous)
     * @return zwraca pusty obiekt osoba 
     */
    public static Person createPerson() {
        return new Person();
    }
    
    /**
     * Metoda ma za zadanie utworzyć i zwrócić obiekt klasy Person (która jest
     * prywatną klasą Miscellaneous)
     * @param name imię osoby
     * @param secondName drugie imię
     * @param surname nazwisko
     * @param pesel numer PESEL
     * @param phones nuery telefonów (jako tablica String[])
     * @param idcard numer dowodu osobistego (z serią)
     * @param active czy osoba nadal jest aktywna (mieszka w danym mieście)
     * @return zwraca obiekt osoba z odpowiednio wypełnionymi danymi
     */    
    public static Person createPerson(String name, String secondName, String surname, String pesel, String[] phones, String idcard, boolean active) {
        return new Person(name, secondName, surname, pesel, phones, idcard, active);
    }
    
    /**
     * Metoda pomocnicza pozwalająca na wyświetlenie daty 
     * @param date obiekt klasy Date (może być utworzony np. przy pomocy setDate)
     * @return ciąg znakowy z odpowiednio sformatowaną datą
     */
    public static String parseDate(Date date) {
            //return SimpleDateFormat.getDateTimeInstance().format(date);// foundationDate.getTime();
            return new SimpleDateFormat("dd-MM-yyyy").format(date);
        }
    
    public static void addPersonToMap(String zip, Person p) {
        PERSONS.put(zip, p);
    }
    
    public static void showAllPersons() {
        for (String k:PERSONS.keys()) {
            System.out.println("==========================");
            PERSONS.get(k).forEach(p->
                    p.getPerson().forEach(
                            (klucz,zawartosc)->
                                    System.out.println("Miasto " + k + ", " + klucz + " " + zawartosc)
                    )
            );
        }
    }
    
    /**
     * Metoda tworzy na podstawie wartości parametrów obiekt Date, przechowujący
     * podaną datę
     * @param date obecnie może być podany jako Long lub String;musi zawierać
     * informacje o dacie:\n
     * a) ciąg znakowy z odpowiednim formatowaniem - dd/MM/yyyy, dd-MM-yyyy, HH:mm, dd-MM-yy d/M/y\n
     * b) czas podany w milisekundach (liczony od epoki Unix - 1970 roku)
     * @return obiekt Date zawierający datę
     * @throws ParseException (jeżeli żaden format nie daty z ciągu znakowego nie będzie pasował)
     */
    public static Date setDate(Object date) throws ParseException {
            Date d = new Date(TimeUnit.NANOSECONDS.toMillis(System.nanoTime()));     
            switch(date.getClass().getSimpleName()) {
                case "String": d= (Date) ((Supplier<Date>) () -> {
                    List<String> patterns = Arrays.asList("dd/MM/yyyy","dd-MM-yyyy","HH:mm", "dd-MM-yy","d/M/y");
                    for (String p:patterns) {    
                        try {
                            return new SimpleDateFormat(p).parse(String.valueOf(date));
                        } catch (ParseException ex) {
                            //Logger.getLogger(Miscellaneous.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    return new Date(TimeUnit.NANOSECONDS.toMillis(System.nanoTime()));
                }).get(); break;
                case "Long":
                case "J": d=new Date(Long.parseLong(String.valueOf(date))); break;
                default: System.err.println("Date mismatched; default TIME was used.");
                //default: d=new Date(System.nanoTime());
            }
            
            return d;
        }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        String str = "5/5/2018";
        Long lng = System.currentTimeMillis();//new Long(TimeUnit.NANOSECONDS.toMillis(System.nanoTime()));
        try {
            System.out.println(Miscellaneous.parseDate(Miscellaneous.setDate(str)));
        } catch (ParseException ex) {
            Logger.getLogger(Miscellaneous.class.getName()).log(Level.SEVERE, null, ex);
        }
        //testowe tworzenie osoby
        //Person p = new Miscellaneous().createPerson();
        //testowe tworzenie osoby z odpowiednimi danymi
        Person p = new Miscellaneous().createPerson("Jacek", "Mariusz", "Waleczko", "8888888888", new String[]{"888-888-888","78765"}, "AAA673956", true);
        
        //przetestowanie funkcji pobierania danych osoby z obiektu
        Map<String,Object> pm = p.getPerson();
        //wyswietlenie danych
        pm.forEach((k,v)->System.out.println(k + " " +v));
        
        //przykład użycia utworzonej multimapy
        MultiMap<String,Integer> myset = new MultiMap<>();
        
        myset.put("pierwsza", 0);
        myset.put("pierwsza", 1);
        myset.put("pierwsza", 3);
        myset.put("aa", 67);
        myset.put("aa", 0);
        
        myset.keys().forEach((s) -> {
            System.out.println(s);
        });
        
        myset.get("pierwsza").forEach((a)-> {
            System.out.println(a);
        });
        //koniec przykładu multimapy
        FileManipulate.writeTextFileLines(((Supplier<List<String>>)()->{
            List<String> ls = new ArrayList<>();
            ls.add("Test1");
            ls.add("Test2");
            ls.add("Test3");
            return ls;
        }).get().stream(), "example.txt");
        
        FileManipulate.readTextFileLines("example.txt").forEach(s->System.out.println(s));
        
        
    }
    
}
